python-astor (0.8.1-4) unstable; urgency=medium

  * Team upload.
  * Add patch to fix failing test (Closes: #1020085)
  * Use dh-sequence-python3
  * Drop old build dependencies
  * Enable autopkgtest-pkg-pybuild
  * Add R³
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 30 Dec 2022 14:59:33 +0100

python-astor (0.8.1-3) unstable; urgency=medium

  [ Tianon Gravi ]
  * Update use of deprecated "-k-" pytest syntax; Closes: #1013726

 -- Sandro Tosi <morph@debian.org>  Sun, 26 Jun 2022 13:15:29 -0400

python-astor (0.8.1-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Sandro Tosi <morph@debian.org>  Thu, 02 Jun 2022 22:22:44 -0400

python-astor (0.8.1-1) unstable; urgency=medium

  * Update to 0.8.1 upstream release
    - https://github.com/berkerpeksag/astor/pull/163 (Closes: #948417)

 -- Tianon Gravi <tianon@debian.org>  Wed, 08 Jan 2020 13:48:12 -0800

python-astor (0.8.0-2) unstable; urgency=medium

  * Team upload.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Sat, 24 Aug 2019 22:33:07 +0200

python-astor (0.8.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Fix Format URL to correct one
  * d/watch: Use https protocol
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

  [ Tianon Gravi ]
  * Update to 0.8.0 upstream release

 -- Tianon Gravi <tianon@debian.org>  Fri, 16 Aug 2019 16:17:32 -0700

python-astor (0.5-1) unstable; urgency=medium

  * Update to 0.5 upstream release.

 -- Tianon Gravi <admwiggin@gmail.com>  Sun, 03 May 2015 00:57:13 +0000

python-astor (0.4-2) unstable; urgency=medium

  * Don't ship the tests module (or setuputils.py).

 -- Tianon Gravi <admwiggin@gmail.com>  Sat, 16 Aug 2014 01:59:12 +0200

python-astor (0.4-1) unstable; urgency=medium

  * Initial release (Closes: #748372).

 -- Tianon Gravi <admwiggin@gmail.com>  Thu, 10 Jul 2014 22:00:13 +0200
